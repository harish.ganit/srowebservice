package sro.webapp.utility;

import java.text.DecimalFormat;
import java.text.ParsePosition;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.script.mustache.SearchTemplateResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import sro.webapp.services.ESCallServices;

@Configuration
public class ESJsonBuilder {

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	ESQueryFormation esQ;
	@Autowired
	SroConstants sroConstants;
	@Autowired
	StandardMethods stMethods;

	//private Logger log = Logger.getLogger(ESJsonBuilder.class);

	public String sroNameJSONBuilder(SearchResponse searchResponse) {
		//log.info("In sroNameJSONBuilder(), fetching required data from search Response");
		JSONObject jObject = new JSONObject(searchResponse);
		JSONArray sronameArr = jObject.getJSONObject("aggregations").getJSONObject("asMap")
				.getJSONObject("distinct_village_code").getJSONArray("buckets");
		return sronameArr.toString();
	}

	public String villageNameJSONBuilder(SearchResponse response) {
		JSONObject jObject = new JSONObject(response);
		JSONArray villagenameArr = jObject.getJSONObject("aggregations").getJSONObject("asMap")
				.getJSONObject("distinct_village_name").getJSONArray("buckets");
		return villagenameArr.toString();
	}

	public String articleNameJSONBuilder(SearchResponse response) {
		JSONObject jObject = new JSONObject(response);
		JSONArray articlenameArr = jObject.getJSONObject("aggregations").getJSONObject("asMap").getJSONObject("my_agg")
				.getJSONArray("buckets");
		return articlenameArr.toString();
	}

	public JSONObject dataJSONBuilder(SearchResponse response) {
		JSONObject jObject = new JSONObject(response);
		if (jObject != null) {
			//log.info("In dataJSONBuilder(), building the json Object from searchResponse");
		}
		JSONObject jResponseObj = new JSONObject(response.toString());
		int totalRecords = jResponseObj.getJSONObject("hits").getJSONObject("total").getInt("value");
		JSONArray fuzzyVOList = jObject.getJSONObject("hits").getJSONArray("hits");
		JSONArray columnArr = new JSONArray();
		DecimalFormat df = new DecimalFormat();
		for (int i = 0; i < fuzzyVOList.length(); i++) {
			try {
				JSONObject jsonobject = fuzzyVOList.getJSONObject(i);
				JSONObject columnVal = new JSONObject();
				jsonobject.getJSONObject("sourceAsMap");
				columnVal.put("FinalRegistrationNumber",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_FinalRegistrationNumber));
				columnVal.put("PropertyDetails",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_PropertyDetails));
				columnVal.put("executant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ExecutantEnglish));
				columnVal.put("claimant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ClaimantEnglish));
				Number num = df.parse(
						(String) (jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_Consideration))
								.toString(),
						new ParsePosition(0));
				columnVal.put("consideration", num.longValue());
				columnVal.put("VillageNameE",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_VillageName));
				columnVal.put("ArticleNameE",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ArticleName));
				columnVal.put("Stamp5Datetime", jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_Date));
				columnVal.put("Claimant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ClaimantKannada));
				columnVal.put("Executant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ExecutantKannada));
				columnVal.put("TotalArea", jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_TotalArea));
				columnArr.put(columnVal);
			} catch (Exception e) {
				System.out.println("ERROR IN : ");
				e.printStackTrace();
			}
		}
		JSONObject finalData = new JSONObject();
		finalData.put("data", columnArr);
		finalData.put("recordsTotal", totalRecords);
		finalData.put("recordsFiltered", totalRecords);
		return finalData;
	}

	public JSONObject fuzzySearchJSONBuilder(SearchTemplateResponse searchResponses) {
		JSONObject jObject = new JSONObject(searchResponses.getResponse());
		System.out.println("--> "+searchResponses.getResponse().toString());
		JSONObject jResponseObj = new JSONObject(searchResponses.getResponse().toString());
		int filteredValue = jResponseObj.getJSONObject("hits").getJSONObject("total").getInt("value");
		JSONArray fuzzyVOList = jObject.getJSONObject("hits").getJSONArray("hits");
		JSONArray columnArr = new JSONArray();
		DecimalFormat df = new DecimalFormat();
		for (int i = 0; i < fuzzyVOList.length(); i++) {
			try {
				JSONObject jsonobject = fuzzyVOList.getJSONObject(i);
				JSONObject columnVal = new JSONObject();
				jsonobject.getJSONObject("sourceAsMap");
				columnVal.put("FinalRegistrationNumber",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_FinalRegistrationNumber));
				columnVal.put("PropertyDetails",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_PropertyDetails));
				columnVal.put("executant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ExecutantEnglish));
				columnVal.put("claimant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ClaimantName));
				Number num = df.parse(
						(String) (jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_Consideration))
								.toString(),
						new ParsePosition(0));
				columnVal.put("consideration", num.longValue());
				columnVal.put("VillageNameE",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_VillageName));
				columnVal.put("ArticleNameE",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ArticleName));
				columnVal.put("Stamp5Datetime", jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_Date));
				columnVal.put("Claimant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ClaimantKannada));
				columnVal.put("Executant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ExecutantKannada));
				columnVal.put("TotalArea", jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_TotalArea));
				columnArr.put(columnVal);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		JSONObject finalData = new JSONObject();
		finalData.put("data", columnArr);
		finalData.put("recordsTotal", filteredValue);
		finalData.put("recordsFiltered", filteredValue);
		return finalData;
	}

	public JSONArray surveyDataTablehJSONBuilder(SearchResponse response, String surveyNumber) {
		JSONObject jObject = new JSONObject(response);
		JSONArray fuzzyVOList = jObject.getJSONObject("hits").getJSONArray("hits");
		JSONArray columnArr = new JSONArray();
		DecimalFormat df = new DecimalFormat();
		for (int i = 0; i < fuzzyVOList.length(); i++) {
			JSONObject jsonobject = fuzzyVOList.getJSONObject(i);
			if (sroConstants.articleNameList
					.contains(jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ArticleName))
					&& stMethods.checkSurveyNumExists(surveyNumber, jsonobject.getJSONObject("sourceAsMap")
							.get(sroConstants.sro_col_PropertyDetails).toString())) {
				JSONObject columnVal = new JSONObject();
				jsonobject.getJSONObject("sourceAsMap");
				columnVal.put("FinalRegistrationNumber",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_FinalRegistrationNumber));
				columnVal.put("PropertyDetails",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_PropertyDetails));
				columnVal.put("executant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ExecutantEnglish));
				columnVal.put("claimant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ClaimantEnglish));
				Number num = df.parse(
						(String) (jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_Consideration))
								.toString(),
						new ParsePosition(0));
				columnVal.put("consideration", num.longValue());
				columnVal.put("VillageNameE",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_VillageName));
				columnVal.put("ArticleNameE",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ArticleName));
				columnVal.put("Stamp5Datetime", jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_Date));
				columnVal.put("Claimant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ClaimantKannada));
				columnVal.put("Executant",
						jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_ExecutantKannada));
				columnVal.put("TotalArea", jsonobject.getJSONObject("sourceAsMap").get(sroConstants.sro_col_TotalArea));
				columnArr.put(columnVal);
			}
		}
		JSONObject finalData = new JSONObject();
		finalData.put("data", columnArr);
		return columnArr;
	}

}
