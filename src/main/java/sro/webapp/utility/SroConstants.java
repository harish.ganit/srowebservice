package sro.webapp.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Configuration;

@Configuration
public class SroConstants {
	
	public final List<String> articleNameList=new ArrayList<String>
	(Arrays.asList("Sale","Joint Development Agreement","Gift deed",
			"Agreement of Sale of Immovable Property (Possession not given)","Lease of immovable property"));
	
	public final String  surveyFormat = "Survey nos.#Survey No.#\r\n" + " Sy Nos#Survey Nos.#\r\n" + " Sy No#\r\n"
			+ " Survey No.#\r\n" + " Sy nos#\r\n" + " Sy No.#\r\n" + " Sy. Nos.#\r\n" + " sy.No.#\r\n" + " Sy. No.#\r\n"
			+ " S No#Sy.No.";

	public final String sro_col_FinalRegistrationNumber="FinalRegistrationNumber";
	
	public final String sro_col_PropertyDetails="PropertyDetails";
	
	public final String sro_col_ExecutantKannada="ExecutantKannada";
	
	public final String sro_col_ClaimantKannada="ClaimantKannada";
	
	public final String sro_col_ExecutantEnglish="ExecutantEnglish";
	
	public final String sro_col_ClaimantEnglish="ClaimantEnglish";
	
	public final String sro_col_ExecutantName="ExecutantName";
	
	public final String sro_col_SimplifiedExecutantName="SimplifiedExecutantName";
	
	public final String sro_col_ClaimantName="ClaimantName";
	
	public final String sro_col_Meta_ExecutantName="Meta_ExecutantName";
	
	public final String sro_col_Meta_SimplifiedExecutantName="Meta_SimplifiedExecutantName";
	
	public final String sro_col_Meta_ClaimantName="Meta_ClaimantName";
	
	public final String sro_col_VillageName="VillageName";
	
	public final String sro_col_VillageName_keyword="VillageName.keyword";
	
	public final String sro_col_ArticleName="ArticleName";
	
	public final String sro_col_ArticleName_keyword="ArticleName.keyword";
	
	public final String sro_col_TotalArea="TotalArea";
	
	public final String sro_col_Unit="Unit";
	
	public final String sro_col_TotalAreaInAcres="TotalAreaInAcres";
	
	public final String sro_col_MarketValue="MarketValue";
	
	public final String sro_col_Consideration="Consideration";
	
	public final String sro_col_SroName="SroName";
	
	public final String sro_col_SroName_keyword="SroName.keyword";
	
	public final String sro_col_FinancialYear="FinancialYear";
	
	public final String sro_col_Date="Date";
	
	public final String sro_col_SurveyNumber="SurveyNumber";
	
	public final String sro_fuzzy_ClaimantName="ClaimantName";
	
	public final String sro_fuzzy_ExecutantName="ExecutantName";
	
	public final String sro_fuzzy_PropertyDetails="PropertyDetails";
}
