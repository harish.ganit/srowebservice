package sro.webapp.utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import sro.webapp.model.DataTableOptions;
import sro.webapp.model.FuzzySearchVO;
import sro.webapp.model.SearchAccordion;

@Configuration
public class StandardMethods {
	@Autowired
	SroConstants sroConstants;

	public DataTableOptions buildDTOptionsObj(HttpServletRequest request) {
		DataTableOptions dtObj = new DataTableOptions();
		dtObj.setDraw(request.getParameter("draw"));
		// dtObj.setGlobalSearch(request.getParameter("isGlobalSearch"));
		dtObj.setLength(Integer.parseInt(request.getParameter("length")));
		dtObj.setSearch(request.getParameter("search[value]"));
		dtObj.setStart(Integer.parseInt(request.getParameter("start")));
		return dtObj;
	}

	public SearchAccordion buildSearchAccordionObj(String searchField, String fy) {
		ObjectMapper mapper = new ObjectMapper();
		SearchAccordion accordion = null;
		String[] fyarr;
		try {
			accordion = mapper.readValue(searchField, SearchAccordion.class);
			if (!(null == fy) && !(fy.equals("null"))) {
				String regex = "\"|\\[|\\]";
				fyarr = fy.replaceAll(regex, "").split(",");
				//accordion.setFyear(Arrays.stream(fyarr).mapToInt(Integer::parseInt).toArray());
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return accordion;
	}

	public FuzzySearchVO buildFuzzySearchObj(HttpServletRequest request, String fieldName, String fieldVal) {
		FuzzySearchVO fuzzyObj = new FuzzySearchVO();
		fuzzyObj.setDraw(request.getParameter("draw"));
		// dtObj.setGlobalSearch(request.getParameter("isGlobalSearch"));
		fuzzyObj.setLength(Integer.parseInt(request.getParameter("length")));
		fuzzyObj.setSearch(request.getParameter("search"));
		fuzzyObj.setStart(Integer.parseInt(request.getParameter("start")));
		fuzzyObj.setFieldName(fieldName);
		fuzzyObj.setFieldVal(fieldVal);
		return fuzzyObj;
	}

	/*Method to check whether the searched Survey Number exists in Property Details or not*/
	public boolean checkSurveyNumExists(String SurveyNum, String propertyDetails) {
		String[] surveyName = sroConstants.surveyFormat.split("#");
		String regex = "(.)*(\\d)(.)*";
		Pattern pattern = Pattern.compile(regex);
		boolean flag = false;
		List<String> resultList = null;
		resultList = new ArrayList<String>();
		for (String z : surveyName) {
			flag = false;
			String[] splity = propertyDetails.split(z.trim());
			for (int i = 0; i < splity.length; i++) {
				if (i % 2 != 0) {
					String[] further = splity[i].trim().split(" ");
					for (String s : further) {
						if (flag == false) {
							String[] slashSeparated = s.split("/");
							if (slashSeparated.length > 0) {
								s = slashSeparated[0];
							}
							if (true == pattern.matcher(s).matches()) {
								resultList.add(s.replaceAll("\\p{Punct}", ""));
							} else {
								if (s.equals("and") || s.equals("&")) {
									flag = false;
								} else {
									flag = true;
								}
							}
						}
					}
				}
			}
		}
		if (resultList.contains(SurveyNum)) {
			return true;
		} else {
			return false;
		}
	}

}
