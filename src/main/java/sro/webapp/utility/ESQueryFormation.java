package sro.webapp.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import sro.webapp.model.DataTableOptions;
import sro.webapp.model.SearchAccordion;

@Configuration
public class ESQueryFormation {
	
	@Autowired
	SroConstants sroConstants;
	
	public final String getSroName="{"
			+ "\"size\": 0,"
			+ "\"aggs\": {\r\n" + 
			"    \"distinct_village_code\": {\r\n" + 
			"      \"terms\": {\r\n" + 
			"        \"field\": \"{{village_code}}\",\r\n" + 
			"        \"size\": {{size}}\r\n" + 
			"      }\r\n" + 
			"    }\r\n" + 
			"  }"
			+ "}";

	public final String getVillageName="{\r\n" + 
			"  \r\n" + 
			"  \"query\":{\r\n" + 
			"    \"bool\":{\r\n" + 
			"      \"must\":[\r\n" + 
			"        {\"match\":{\"{{village_code}}\":\"{{vcodeVal}}\"}}\r\n" + 
			"        ]\r\n" + 
			"    }\r\n" + 
			"  },\r\n" + 
			"  \"size\": 0,\r\n" + 
			"  \"aggs\": {\r\n" + 
			"    \"distinct_village_name\": {\r\n" + 
			"      \"terms\": {\r\n" + 
			"        \"field\": \"{{VillageNameE}}\",\r\n" + 
			"        \"size\": {{size}}\r\n" + 
			"      }\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"}";
	
	public final String getArticleName="{\r\n" + 
			"  \"query\":{\r\n" + 
			"    \"bool\":{\r\n" + 
			"      \"must\":[\r\n" + 
			"        {\"match\":{\"{{village_code}}\":\"{{vcodeVal}}\"}},\r\n" + 
			"        {\"match\":{\"{{VillageNameE}}\":\"{{villagenameVal}}\"}}\r\n" + 
			"        ]\r\n" + 
			"    }\r\n" + 
			"  },\r\n" + 
			"  \"size\": 0,\r\n" + 
			"  \"aggregations\": {\r\n" + 
			"    \"my_agg\": {\r\n" + 
			"      \"terms\": {\r\n" + 
			"        \"field\": \"{{ArticleNameEkeyword}}\",\r\n" + 
			"        \"size\": {{size}}\r\n" + 
			"      }\r\n" + 
			"      \r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"}";

	public final String getClaimantFuzzyQuery="{\r\n" + 
			"  \"from\" : {{from}}, \"size\" : {{size}},\r\n" + 
			"  \"track_total_hits\": true,\r\n" + 
			"    \"query\": {\r\n" + 
			"        \"fuzzy\" : {\r\n" + 
			"            \"ClaimantName\" : {\r\n" + 
			"                \"value\": \"{{fuzzySearchKey}}\",\r\n" + 
			"                \"boost\": 2.0,\r\n" + 
			"                \"fuzziness\": 2\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    }\r\n" + 
			"}";
	/*
	 * public final String getClaimantMultiWordsQuery="{\r\n" +
	 * "  \"from\" : {{from}}, \"size\" : {{size}},\r\n" + "  \"query\": {\r\n" +
	 * "    \"multi_match\": {\r\n" + "      \"query\": \"{{fuzzySearchKey}}\",\r\n"
	 * + "      \"fields\": [\"{{ClaimantName}}\",\"{{ClaimantName1}}\"],\r\n" +
	 * "      \"fuzziness\": \"1\"\r\n" + "    }\r\n" + "  },\r\n" +
	 * "    \"highlight\" : {\r\n" + "        \"fields\" : {\r\n" +
	 * "            \"ClaimantName\" : {\r\n" +
	 * "              \"highlight_query\": {\r\n" +
	 * "                \"multi_match\": {\r\n" +
	 * "      \"query\": \"{{fuzzySearchKey1}}\",\r\n" +
	 * "      \"fields\": [\"{{ClaimantName2}}\",\"{{3}}\"],\r\n" +
	 * "      \"fuzziness\": 1" + "    }}\r\n" + "            }\r\n" +
	 * "        }\r\n" + "    }\r\n" + "}";
	 */
	public final String getClaimantMultiWordsQuery="{\r\n" + 
			"  \"from\": {{from}}, \"size\": {{size}}, \r\n" + 
			"   \"query\": {\r\n" + 
			"  \"multi_match\" : {\r\n" + 
			"      \"query\":    \"{{fuzzySearchKey}}\", \r\n" + 
			"      \"fields\": [ \"{{ClaimantName}}\", \"{{ClaimantName}}\" ] ,\r\n" + 
			"                \"boost\": 2.0,\r\n" + 
			"                \"fuzziness\": 1\r\n" + 
			"    }\r\n" + 
			"  }\r\n" + 
			"}";
	public final String getExecutantFuzzyQuery="{\r\n" + 
			"  \"from\" : {{from}}, \"size\" : {{size}},\r\n" + 
			"  \"track_total_hits\": true,\r\n" + 
			"    \"query\": {\r\n" + 
			"        \"fuzzy\" : {\r\n" + 
			"            \"ExecutantName\" : {\r\n" + 
			"                \"value\": \"{{fuzzySearchKey}}\",\r\n" + 
			"                \"boost\": 2.0,\r\n" + 
			"                \"fuzziness\": 2\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    }\r\n" + 
			"}";
	public final String getPropertyFuzzyQuery="{\r\n" + 
			"  \"from\" : {{from}}, \"size\" : {{size}},\r\n" + 
			"  \"track_total_hits\": true,\r\n" + 
			"    \"query\": {\r\n" + 
			"        \"fuzzy\" : {\r\n" + 
			"            \"PropertyDetails\" : {\r\n" + 
			"                \"value\": \"{{fuzzySearchKey}}\",\r\n" + 
			"                \"boost\": 2.0,\r\n" + 
			"                \"fuzziness\": 2\r\n" + 
			"            }\r\n" + 
			"        }\r\n" + 
			"    }\r\n" + 
			"}";
	public final String surveyNumberQuery="{\"size\" : {{sizeVal}},\r\n" + 
			"    \"query\": {\r\n" + 
			"        \"bool\":{\r\n" + 
			"      \"must\":[\r\n" + 
			"        {\"match\":{\"{{VillageNameE}}\":\"{{villageNameVal}}\"}}\r\n" + 
			"        ]\r\n" + 
			"    }\r\n" + 
			"    }\r\n" + 
			"}";
	public String getDataTableQuery(DataTableOptions dtoptions, SearchAccordion accordion) {
		StringBuilder br= new StringBuilder();
		br.append("{"
				+ "\"from\" : {{from}}, \"size\" : {{size}},"
				+ "\"track_total_hits\": true,"
				+ "\"query\" : {");
				
		if(!(null==accordion)) {
			br.append("\"bool\" : {"
				+ "\"must\" :[");
			if(!(dtoptions.getSearch().isEmpty()) && !(dtoptions.getSearch()==null)) {
				br.append("{\"match\" :{\"{{message}}\": \"{{messageVal}}\"}}");
			}
		if(!accordion.getSroname().equals("null")&& !accordion.getSroname().isEmpty()) {
			br.append("{\"match\" :{\"{{villagecode}}\":\"{{vcodeVal}}\"}}");
			if(null!=accordion.getVillageName() && !accordion.getVillageName().isEmpty()) {
				br.append(",{\"match\" :{\"{{VillageNameE}}\":\"{{villagenameVal}}\"}}");
				if(null!=accordion.getArticleName() && !accordion.getArticleName().isEmpty()) {
					br.append(",{\"match\" :{\"{{ArticleNameE}}\":\"{{ArticleNameEVal}}\"}}");
				}
			}
		}
		if((null!=accordion.getAmountFrom() && accordion.getAmountFrom()>0) || (null!=accordion.getAmountTill() && accordion.getAmountTill() > 0)) {
			br.append(",{"
					+ "\"range\" :{"
					+ "\"Consideration\" :{");
			if(null!=accordion.getAmountFrom() && accordion.getAmountFrom()>0) {
				br.append("\"gte\" :{{amountFrom}}");
			}
			if(null!=accordion.getAmountTill() && accordion.getAmountTill() > 0) {
				br.append("\"gte\" :{{amountTill}}");
			}
			br.append(",\"boost\" :2.0");
			br.append("}"
					+ "}"
					+ "}");
		}
		
		br.append("]"
				+ "}");
		}
		else {
			if(!(dtoptions.getSearch().isEmpty()) && !(null==dtoptions.getSearch())) {
				br.append("\"bool\" : {"
						+ "\"must\" :[");
				br.append("{\"match\" :{\"{{message}}\": \"{{messageVal}}\"}}]}");
				
			}
			else {
			br.append("\"match_all\" :{}");
			}
		}
		
		br.append("}");
		br.append("}");
		return br.toString();
	}
	
	public String getFuzzySearchOnColSearchedQuery(String fieldName,String villageDropSelectedVal,
			String articleDropSelectedVal, String claimantSearch, String executantSearch, String propertySearch) {
		StringBuilder query=new StringBuilder();
		try {
			query.append("{"
					+ "\"from\" : {{from}}, \"size\" : {{size}},"
					+ "\"query\" :{"
					+ "\"bool\" :{"
					+ "\"must\" :[");
			if(fieldName.equals(sroConstants.sro_fuzzy_ClaimantName)) {
				query.append("{"
						+ "\"fuzzy\" :{"
						+ "\"{{ClaimantName}}\":\"{{ClaimantNameVal}}\""
						+ "}"
						+ "},");
			}
			else if(fieldName.equals(sroConstants.sro_fuzzy_ExecutantName)) {
				query.append("{"
						+ "\"fuzzy\" :{"
						+ "\"{{ExecutantName}}\":\"{{ExecutantNameVal}}\""
						+ "}"
						+ "},");
			}
			else if(fieldName.equals(sroConstants.sro_fuzzy_PropertyDetails)) {
				query.append("{"
						+ "\"fuzzy\" :{"
						+ "\"{{PropertyDetails}}\":\"{{PropertyDetailsVal}}\""
						+ "}"
						+ "},");
			}
			if(!(null==villageDropSelectedVal) && villageDropSelectedVal!="" && !villageDropSelectedVal.equals("undefined")) {
				query.append("{"
						+ "\"term\" :{"
						+ "\"{{VillageName}}\":\"{{VillageNameVal}}\""
						+ "}"
						+ "}");
			}
			if(!(null==articleDropSelectedVal) && articleDropSelectedVal!="") {
				if(!(null==villageDropSelectedVal) && (!villageDropSelectedVal.isEmpty()) && !villageDropSelectedVal.equals("undefined")) {
					query.append(",");
				}
				query.append("{"
						+ "\"term\" :{"
						+ "\"{{ArticleName}}\":\"{{ArticleNameVal}}\""
						+ "}"
						+ "}");
			}
			if(!(null==claimantSearch) && claimantSearch !="") {
				if(!(null==articleDropSelectedVal) && articleDropSelectedVal!="") {
					query.append(",");
				}
				else if(!articleDropSelectedVal.isEmpty()) {
					query.append(",");
				}
				query.append("{"
						+ "\"match\" :{"
						+ "\"{{ClaimantName}}\":\"{{ClaimantNameVal}}\""
						+ "}"
						+ "}");
			}
			if(!(null==executantSearch) && executantSearch!="") {
				if(!claimantSearch.isEmpty() ) {
					query.append(",");
				}
				else if(!articleDropSelectedVal.isEmpty()) {
					query.append(",");
				}
				else if(!villageDropSelectedVal.isEmpty() && !villageDropSelectedVal.equals("undefined")) {
					query.append(",");
				}
				query.append("{"
						+ "\"match\" :{"
						+ "\"{{ExecutantName}}\":\"{{ExecutantNameVal}}\""
						+ "}"
						+ "}");
			}
			if(claimantSearch.isEmpty()) {
				System.out.println("empty");
			}
			if(!(null==propertySearch) && propertySearch !="") {
				if(!executantSearch.isEmpty()) {
					query.append(",");
				}else if(!claimantSearch.isEmpty() ) {
					query.append(",");
				}else if(!articleDropSelectedVal.isEmpty()) {
					query.append(",");
				}
				else if(!villageDropSelectedVal.isEmpty() && !villageDropSelectedVal.equals("undefined")) {
					query.append(",");
				}
				query.append("{"
						+ "\"match\" :{"
						+ "\"{{PropertyDetails}}\":\"{{PropertyDetailsVal}}\""
						+ "}"
						+ "}");
			}
			query.append("]"
					+ "}"
					+ "}"
					+ "}");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println(query.toString());
		return query.toString();
	}
}
