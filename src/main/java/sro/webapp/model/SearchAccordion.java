package sro.webapp.model;

import java.util.Arrays;

public class SearchAccordion {
	private String sroname;
	private String villageName;
	private String articleName;
	private Long amountFrom;
	private Long amountTill;
	private int[] fyear;
	public String getSroname() {
		return sroname;
	}
	public void setSroname(String sroname) {
		this.sroname = sroname;
	}
	public String getVillageName() {
		return villageName;
	}
	public void setVillageName(String villageName) {
		this.villageName = villageName;
	}
	public String getArticleName() {
		return articleName;
	}
	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}
	public Long getAmountFrom() {
		return amountFrom;
	}
	public void setAmountFrom(Long amountFrom) {
		this.amountFrom = amountFrom;
	}
	public Long getAmountTill() {
		return amountTill;
	}
	public void setAmountTill(Long amountTill) {
		this.amountTill = amountTill;
	}
	public int[] getFyear() {
		return fyear;
	}
	public void setFyear(int[] fyear) {
		this.fyear = fyear;
	}
	@Override
	public String toString() {
		return "SearchAccordion [sroname=" + sroname + ", villageName=" + villageName + ", articleName=" + articleName
				+ ", amountFrom=" + amountFrom + ", amountTill=" + amountTill + ", fyear=" + Arrays.toString(fyear)
				+ "]";
	}
	
	

}
