package sro.webapp.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.script.mustache.SearchTemplateRequest;
import org.elasticsearch.script.mustache.SearchTemplateResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import sro.webapp.model.DataTableOptions;
import sro.webapp.model.FuzzySearchVO;
import sro.webapp.model.SearchAccordion;
import sro.webapp.services.ESCallServices;
import sro.webapp.services.SroServices;
import sro.webapp.utility.StandardMethods;

@Controller
@CrossOrigin(origins = "http:localhost:4200/ganit", allowedHeaders = "*")
public class SroFrontControler {

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	ESCallServices esService;
	@Autowired
	SroServices sroService;
	@Autowired
	StandardMethods stMethods;

	static String searchField;
	static String fy;
	static int some = 0;
	//private Logger log = Logger.getLogger(SroFrontControler.class);

	/*Validate the login credentials based on the value provided from prop file and return the success message*/
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@GetMapping(value = "/authenticateUser", produces = "application/json")
	public ResponseEntity<String> validateLoginUser(@RequestHeader("username") String username,
			@RequestHeader("password") String password) {
		//log.info("In validateLoginUser() to authenticate username and password");
		String usernamedecoded = new String(Base64.getDecoder().decode(username));
		String passwordecoded = new String(Base64.getDecoder().decode(password));
		JSONObject authenticateMessage = new JSONObject();
		authenticateMessage.put("message", sroService.authenticate(usernamedecoded.trim(), passwordecoded.trim()));
		return ResponseEntity.status(HttpStatus.OK).body(authenticateMessage.toString());
	}

	/* Fetch the Unique SRO Name when the search page loads*/
	@GetMapping(value = "/getsronamelist", produces = "application/json")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> produceSroNameList() {
		//log.info("In produceSroNameList(), to get the SRO Names in dropdown");
		return ResponseEntity.status(HttpStatus.OK).body(esService.getSROName());
	}

	/*Get the unique village name based on Sro Name input provided*/
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@GetMapping(value = "/getVillageName", produces = "application/json")
	public ResponseEntity<String> produceVillageName(@RequestParam("sroname") String sroName) throws IOException {
		//log.info("In produceVillageName(), to get the village name for SRO (" + sroName + ")");
		return ResponseEntity.status(HttpStatus.OK).body(esService.getVillageName(sroName));

	}

	/*Get the Article Name based on the input provided for field : SroName and Village Name*/
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@GetMapping(value = "/getArticleNameList", produces = "application/json")
	public ResponseEntity<String> produceArticleName(@RequestParam("sroname") String sroName,
			@RequestParam("villagename") String villageName) {
		//log.info("In produceArticleName(), to get the Article name based on Village (" + villageName + ")");
		return ResponseEntity.status(HttpStatus.OK).body(esService.getArticleName(sroName, villageName));
	}

	@PostMapping(value = "/saveSearchAccordion", produces = "application/json")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> getSroDataTable(@RequestParam("searchaccord") String searchaccord,
			@RequestParam(value = "fyear", required = false) String fyear) {
		//log.info("In getSroDataTable(), to get the Sro Data to display in Datatable from 0 to 100");
		searchField = searchaccord;
		fy = fyear;
		return ResponseEntity.status(HttpStatus.OK).body("{\"success\":\"success\"}");
	}

	@GetMapping(value = "getFuzzyResult", produces = "application/json")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> getFuzzyResult(HttpServletRequest request,
			@RequestParam("fieldName") String fieldName, @RequestParam("fieldValue") String fieldVal) {
		System.out.println("fieldName: "+fieldName+" &fieldVal: "+fieldVal);
		//log.info("In getFuzzyResult(), to display the fuzzy result based on search keyword: '" + fieldVal + "'"+ " AND Field Name : " + fieldName);
		FuzzySearchVO fuzzyVO = stMethods.buildFuzzySearchObj(request, fieldName, fieldVal);
		JSONObject getFuzzyDataObj = esService.getSearchedFuzzyResult(fuzzyVO);
		return ResponseEntity.status(HttpStatus.OK).body(getFuzzyDataObj.toString());
	}

	@GetMapping(value = "sampleinput", produces = "application/json")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> getDatatbleAll(HttpServletRequest request) throws IOException {
		DataTableOptions dtoptions = new DataTableOptions();
		SearchAccordion accordion = new SearchAccordion();
		dtoptions = stMethods.buildDTOptionsObj(request);
		accordion = stMethods.buildSearchAccordionObj(searchField, fy);
		JSONObject result = esService.getDatatbleData(dtoptions, accordion);
		return ResponseEntity.status(HttpStatus.OK).body(result.toString());
	}

	@GetMapping(value = "/surveyDataTable", produces = "application/json")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> getSurveyNumberDataTable(@RequestParam("surveyNumber") String surveyNumber) {
		//log.info("In getSurveyNumberDataTable(), searching data for Searched Survey number=" + surveyNumber);
		return ResponseEntity.status(HttpStatus.OK)
				.body(sroService.getSurveyNumberDataTable(surveyNumber, "Bellanduru").toString());
	}
	
	@GetMapping(value="/searchOnFuzzyRecords", produces="application/json")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> searchOnFuzzyRecords(HttpServletRequest request,
			@RequestParam("fieldName") String fieldName, @RequestParam("fieldValue") String fieldValue
			,@RequestParam(value="villageDropSelectedVal",required=false)String villageDropSelectedVal,
			@RequestParam(value="articleDropSelectedVal", required=false) String articleDropSelectedVal,
			@RequestParam(value="claimantSearch", required= false)String claimantSearch,
			@RequestParam(value="executantSearch", required=false) String executantSearch,
			@RequestParam(value="propertySearch", required=false) String propertySearch){
		System.out.println("villageDropSelectedVal: "+villageDropSelectedVal+" articleDropSelectedVal: "+articleDropSelectedVal);
		System.out.println("propertySearch : "+propertySearch);
		//log.info("In searchOnFuzzyRecords(), To get the Further filter Value based above input field");
		FuzzySearchVO fuzzyVO = stMethods.buildFuzzySearchObj(request, fieldName, fieldValue);
		JSONObject getFuzzyDataObjColSearch = esService.getFuzzyColumnSortData(fuzzyVO,villageDropSelectedVal,articleDropSelectedVal,claimantSearch,
				executantSearch,propertySearch);
		return ResponseEntity.status(HttpStatus.OK).body(getFuzzyDataObjColSearch.toString());
	}

	@GetMapping(value = "/datatble", produces = "application/json")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> getData(@RequestParam("searchacord") String data) {
		// JSONObject dataTableObj = new JSONObject(data);
		System.out.println("search :" + data);
		JSONObject temp = new JSONObject();
		temp.put("id", "1");
		temp.put("firstName", "rahman");
		temp.put("lastName", "rahman");
		JSONObject temp1 = new JSONObject();
		temp1.put("id", "2");
		temp1.put("firstName", "msbah");
		temp1.put("lastName", "rahman");
		JSONArray tempa = new JSONArray();
		tempa.put(temp);
		tempa.put(temp1);
		JSONObject fin = new JSONObject();
		if (some > 0) {
			fin.put("data", tempa);
		} else {

		}
		System.out.println(some);
		some++;
		fin.put("recordsTotal", 24);
		fin.put("recordsFiltered", 24);
		return ResponseEntity.status(HttpStatus.OK).body(fin.toString());
	}

	@GetMapping(value = "/getec")
	public ResponseEntity<String> getAllMatchedQuery() throws URISyntaxException, IOException {
		/*
		 * JsonObject ecquery=new JsonObject(); JsonObject match=new JsonObject();
		 * match.addProperty("match_all", "{}"); ecquery.add("query", match);
		 * System.out.println("---> "+ecquery.toString()); HttpHeaders headers = new
		 * HttpHeaders(); headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		 * 
		 * final String baseUrl = "http://localhost:9200/sro_data/sro_entries/_search";
		 * URI uri = new URI(baseUrl); HttpEntity<String> entity = new
		 * HttpEntity<String>(headers); String res=restTemplate.postForEntity( uri,
		 * ecquery.toString(), String.class).getBody();
		 */

		RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200,
				"http")));/*
							 * SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
							 * QueryBuilder qbm = QueryBuilders.matchQuery("village_code", "CKB");
							 * 
							 * searchSourceBuilder.query(qbm);
							 * 
							 * SearchRequest searchRequest = new SearchRequest();
							 * searchRequest.indices("sro_data"); searchRequest.source(searchSourceBuilder);
							 * System.out.println("---->"+searchSourceBuilder.toString()); SearchResponse
							 * searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
							 */

		SearchTemplateRequest request = new SearchTemplateRequest();
		request.setRequest(new SearchRequest("sro_data"));

		request.setScriptType(ScriptType.INLINE);
		request.setScript("{" + "\"_source\": \"{{field1}}\"," + "\"query\": { " + "\"bool\": {" + "\"filter\": [ "
				+ "{ \"term\":  { \"{{field3}}\": \"{{value}}\" }}" + "]" + "}" + "}," + "  \"size\" : \"{{size}}\","
				+ "\"aggs\":{\r\n" + "        \"unique_village_code\": {\r\n" + "            \"terms\": {\r\n"
				+ "                \"field\": \"{{uniq}}\"\r\n" + "            }\r\n" + "        }\r\n" + "    }"
				+ "}");
		System.out.println("---> " + request.getScript());
		Map<String, Object> scriptParams = new HashMap<String, Object>();
		scriptParams.put("field1", "VillageNameE");
		// scriptParams.put("field2", "VillageNameE");
		scriptParams.put("field3", "village_code");// village_code
		scriptParams.put("uniq", "village_code");// village_code
		scriptParams.put("value", "ckb");
		scriptParams.put("size", 1000);
		request.setScriptParams(scriptParams);

		SearchTemplateResponse searchResponse = client.searchTemplate(request, RequestOptions.DEFAULT);

		return ResponseEntity.status(HttpStatus.OK).body(searchResponse.toString());
	}

}
