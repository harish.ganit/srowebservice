package sro.webapp.services;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Configuration
public class SroServices {
	
	@Autowired Environment env;
	@Autowired ESCallServices esService;
	
	public JSONArray getSurveyNumberDataTable(String surveyNumber, String villageName) {
		JSONArray surveyTableObj=esService.getSurveyDataTable(villageName,surveyNumber);
		return surveyTableObj;
	}
	
	public String authenticate(String username, String password) {
		if(username.equals(env.getProperty("sro.username")) && password.equals(env.getProperty("sro.password"))) {
			return "success";
		}
		else {
			return "fail";
		}
		
	}

}
