package sro.webapp.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.script.mustache.SearchTemplateRequest;
import org.elasticsearch.script.mustache.SearchTemplateResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import sro.webapp.controller.SroFrontControler;
import sro.webapp.model.DataTableOptions;
import sro.webapp.model.FuzzySearchVO;
import sro.webapp.model.SearchAccordion;
import sro.webapp.utility.ESJsonBuilder;
import sro.webapp.utility.ESQueryFormation;
import sro.webapp.utility.SroConstants;

@Configuration
public class ESCallServices {

	@Autowired
	ESQueryFormation esq;
	@Autowired
	Environment env;
	@Autowired
	RestHighLevelClient restClient;
	@Autowired
	ESJsonBuilder jsonBulder;
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	SroConstants sroConstants;

	//private Logger log = Logger.getLogger(ESCallServices.class);

	public String getSROName() {
		String res = null;
		SearchTemplateResponse searchResponse = null;
		SearchTemplateRequest request = new SearchTemplateRequest();
		request.setRequest(new SearchRequest(env.getProperty("es.index.name")));
		request.setScriptType(ScriptType.INLINE);
		request.setScript(esq.getSroName);
		Map<String, Object> scriptParams = new HashMap<String, Object>();
		scriptParams.put("village_code", sroConstants.sro_col_SroName);
		scriptParams.put("size", 1000);
		request.setScriptParams(scriptParams);
		try {
			searchResponse = restClient.searchTemplate(request, RequestOptions.DEFAULT);
			//log.info("In getSROName(), sending searchTemplate Response for JSON response building");
			res = jsonBulder.sroNameJSONBuilder(searchResponse.getResponse());
//			if (res != null) {
//				//log.info("JSON Response build in completed");
//			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return res;
	}

	public String getVillageName(String sroName) throws IOException {
		String result = null;
		SearchTemplateResponse searchResponse = null;
		SearchTemplateRequest request = new SearchTemplateRequest();
		try {
			request.setRequest((new SearchRequest(env.getProperty("es.index.name"))));
			request.setScriptType(ScriptType.INLINE);
			request.setScript(esq.getVillageName);
			Map<String, Object> scriptParams = new HashMap<String, Object>();
			scriptParams.put("village_code", sroConstants.sro_col_SroName);
			scriptParams.put("vcodeVal", sroName.toUpperCase());
			scriptParams.put("VillageNameE", sroConstants.sro_col_VillageName);
			scriptParams.put("size", 1000);
			request.setScriptParams(scriptParams);
			searchResponse = restClient.searchTemplate(request, RequestOptions.DEFAULT);
			result = jsonBulder.villageNameJSONBuilder(searchResponse.getResponse());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		return result;
	}

	public String getArticleName(String sroName, String villageName) {
		String result = null;
		SearchTemplateResponse searchResponse = null;
		SearchTemplateRequest request = new SearchTemplateRequest();
		try {
			request.setRequest((new SearchRequest(env.getProperty("es.index.name"))));
			request.setScriptType(ScriptType.INLINE);
			request.setScript(esq.getArticleName);
			Map<String, Object> scriptParams = new HashMap<String, Object>();
			scriptParams.put("village_code", sroConstants.sro_col_SroName);
			scriptParams.put("vcodeVal", sroName);
			scriptParams.put("VillageNameE", sroConstants.sro_col_VillageName);
			scriptParams.put("villagenameVal", villageName);
			scriptParams.put("ArticleNameEkeyword", sroConstants.sro_col_ArticleName);
			scriptParams.put("size", 100);
			request.setScriptParams(scriptParams);
			searchResponse = restClient.searchTemplate(request, RequestOptions.DEFAULT);
			result = jsonBulder.articleNameJSONBuilder(searchResponse.getResponse());
		} catch (Exception e) {
			e.getMessage();
		} finally {

		}
		return result;
	}

	public JSONObject getDatatbleData(DataTableOptions dtoptions, SearchAccordion accordion) throws IOException {
		JSONObject result = null;
		SearchTemplateResponse searchResponse = null;
		SearchTemplateRequest request = new SearchTemplateRequest();
		request.setRequest((new SearchRequest(env.getProperty("es.index.name"))));
		request.setScriptType(ScriptType.INLINE);
		request.setScript(esq.getDataTableQuery(dtoptions, accordion));
		Map<String, Object> scriptParams = new HashMap<String, Object>();
		scriptParams.put("from", dtoptions.getStart());
		scriptParams.put("size", dtoptions.getLength());
		if (!(null == accordion)) {
			if (!(dtoptions.getSearch().isEmpty()) && !(dtoptions.getSearch() == null)) {
				scriptParams.put("message", "message");
				scriptParams.put("messageVal", dtoptions.getSearch());
			}
			if (null != accordion.getSroname() && !accordion.getSroname().isEmpty()) {
				scriptParams.put("villagecode", sroConstants.sro_col_SroName);
				scriptParams.put("vcodeVal", accordion.getSroname());
				if (null != accordion.getVillageName() && !accordion.getVillageName().isEmpty()) {
					scriptParams.put("VillageNameE", sroConstants.sro_col_VillageName);
					scriptParams.put("villagenameVal", accordion.getVillageName());
					if (null != accordion.getArticleName() && !accordion.getArticleName().isEmpty()) {
						scriptParams.put("ArticleNameE", sroConstants.sro_col_ArticleName);
						scriptParams.put("ArticleNameEVal", accordion.getArticleName());
					}
				}
			}
			if ((null != accordion.getAmountFrom() && accordion.getAmountFrom() > 0)
					|| (null != accordion.getAmountTill() && accordion.getAmountTill() > 0)) {
				if (null != accordion.getAmountFrom() && accordion.getAmountFrom() > 0) {
					scriptParams.put("amountFrom", accordion.getAmountFrom());
				}
				if (null != accordion.getAmountTill() && accordion.getAmountTill() > 0) {
					scriptParams.put("amountTill", accordion.getAmountTill());
				}
			}
		}
		if (!(dtoptions.getSearch().isEmpty()) && !(dtoptions.getSearch() == null)) {
			scriptParams.put("message", "message");
			scriptParams.put("messageVal", dtoptions.getSearch());
		}
		request.setScriptParams(scriptParams);
		searchResponse = restClient.searchTemplate(request, RequestOptions.DEFAULT);
		result = jsonBulder.dataJSONBuilder(searchResponse.getResponse());
		return result;
	}

	public JSONObject getSearchedFuzzyResult(FuzzySearchVO fuzzyObj) {
		JSONObject result = null;
		SearchTemplateResponse searchResponse = null;
		SearchTemplateRequest request = new SearchTemplateRequest();
		try {
			request.setRequest((new SearchRequest(env.getProperty("es.index.name"))));
			String searchColumn="";
			if (sroConstants.sro_fuzzy_ClaimantName.equals(fuzzyObj.getFieldName())) {
				request.setScript(esq.getClaimantMultiWordsQuery);
				searchColumn="ClaimantName";
				System.out.println(esq.getClaimantMultiWordsQuery);
			} else if (sroConstants.sro_fuzzy_ExecutantName.equals(fuzzyObj.getFieldName())) {
				request.setScript(esq.getExecutantFuzzyQuery);
				searchColumn="ExecutantName";
			} else if (sroConstants.sro_fuzzy_PropertyDetails.equals(fuzzyObj.getFieldName())) {
				request.setScript(esq.getPropertyFuzzyQuery);
				searchColumn="PropertyDetails";
			}
			request.setScriptType(ScriptType.INLINE);
			//Map<String, HighlightField> scriptParams = new HashMap<String, HighlightField>();
			Map<String, Object> scriptParams = new HashMap<String, Object>();
			scriptParams.put("from", fuzzyObj.getStart());
			scriptParams.put("size", fuzzyObj.getLength());
			scriptParams.put("fuzzySearchKey", fuzzyObj.getFieldVal());
			scriptParams.put("ClaimantName", searchColumn);
			request.setScriptParams(scriptParams);
			searchResponse = restClient.searchTemplate(request, RequestOptions.DEFAULT);
			result = jsonBulder.fuzzySearchJSONBuilder(searchResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public JSONArray getSurveyDataTable(String villageName, String surveyNumber) {
		JSONArray result = null;
		SearchTemplateResponse searchResponse = null;
		SearchTemplateRequest request = new SearchTemplateRequest();
		try {
			request.setRequest((new SearchRequest(env.getProperty("es.index.name"))));
			request.setScriptType(ScriptType.INLINE);
			request.setScript(esq.surveyNumberQuery);
			Map<String, Object> scriptParams = new HashMap<String, Object>();
			scriptParams.put("sizeVal", 10000);
			scriptParams.put("VillageNameE", sroConstants.sro_col_VillageName);
			scriptParams.put("villageNameVal", villageName);
			request.setScriptParams(scriptParams);
			searchResponse = restClient.searchTemplate(request, RequestOptions.DEFAULT);
			result = jsonBulder.surveyDataTablehJSONBuilder(searchResponse.getResponse(), surveyNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public JSONObject getFuzzyColumnSortData(FuzzySearchVO fuzzyVO, String villageDropSelectedVal,
			String articleDropSelectedVal, String claimantSearch, String executantSearch, String propertySearch) {
		JSONObject result = null;
		SearchTemplateResponse searchResponse = null;
		SearchTemplateRequest request = new SearchTemplateRequest();
		try {
			request.setRequest((new SearchRequest(env.getProperty("es.index.name"))));
			request.setScriptType(ScriptType.INLINE);
			request.setScript(esq.getFuzzySearchOnColSearchedQuery(fuzzyVO.getFieldName(), villageDropSelectedVal, articleDropSelectedVal,claimantSearch,
					executantSearch,propertySearch));
			Map<String, Object> scriptParams = new HashMap<String, Object>();
			if(fuzzyVO.getFieldName().equals(sroConstants.sro_fuzzy_ClaimantName)) {
				scriptParams.put("ClaimantName", sroConstants.sro_fuzzy_ClaimantName);
				scriptParams.put("ClaimantNameVal", fuzzyVO.getFieldVal());
			}
			else if(fuzzyVO.getFieldName().equals(sroConstants.sro_fuzzy_ExecutantName)) {
				scriptParams.put("ExecutantName", sroConstants.sro_fuzzy_ExecutantName);
				scriptParams.put("ExecutantNameVal", fuzzyVO.getFieldVal());
			}
			else if(fuzzyVO.getFieldName().equals(sroConstants.sro_fuzzy_PropertyDetails)) {
				scriptParams.put("PropertyDetails", sroConstants.sro_fuzzy_PropertyDetails);
				scriptParams.put("PropertyDetailsVal", fuzzyVO.getFieldVal());
			}
			if(!(null==villageDropSelectedVal) && villageDropSelectedVal!="" && !villageDropSelectedVal.equals("undefined")) {
				scriptParams.put("VillageName", sroConstants.sro_col_VillageName);
				scriptParams.put("VillageNameVal", villageDropSelectedVal);
			}
			if(!(null==articleDropSelectedVal) && articleDropSelectedVal!="") {
				scriptParams.put("ArticleName",sroConstants.sro_col_ArticleName);
				scriptParams.put("ArticleNameVal", articleDropSelectedVal);
			}
			if(!(null==claimantSearch) && claimantSearch!="") {
				scriptParams.put("ClaimantName", sroConstants.sro_col_ClaimantName);
				scriptParams.put("ClaimantNameVal", claimantSearch);
			}
			if(!(null==executantSearch) && executantSearch!="") {
				scriptParams.put("ExecutantName", sroConstants.sro_col_ExecutantName);
				scriptParams.put("ExecutantNameVal", executantSearch);
			}
			if(!(null==propertySearch) && propertySearch!="") {
				scriptParams.put("PropertyDetails", sroConstants.sro_col_PropertyDetails);
				scriptParams.put("PropertyDetailsVal", executantSearch);
			}
			scriptParams.put("from", fuzzyVO.getStart());
			scriptParams.put("size", fuzzyVO.getLength());
			request.setScriptParams(scriptParams);
			searchResponse = restClient.searchTemplate(request, RequestOptions.DEFAULT);
			result = jsonBulder.fuzzySearchJSONBuilder(searchResponse);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
